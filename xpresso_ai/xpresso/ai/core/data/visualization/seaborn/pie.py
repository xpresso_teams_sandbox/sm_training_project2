import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch
from xpresso.ai.core.commons.utils.constants import PIE_CHART_OTHERS_THRESHOLD


class NewPlot(Plot):
    """
    Generates a pie plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): Data for plotting
        input_2(list): Labels of each category
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """
    def __init__(self, input_1, input_2, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH,
                 threshold=PIE_CHART_OTHERS_THRESHOLD):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels
        if len(input_1) != len(input_2):
            raise InputLengthMismatch
        self.figure, self.plot = plt.subplots(figsize=self.figure_size)
        self.plot.set(title=plot_title)
        data = pd.DataFrame(list(zip(input_1, input_2)))
        data = self.club_small_categories(data, threshold)
        self.plot.pie(x=data[0], labels=data[1],
                      autopct=lambda p: '{:.3f}%'.format(p),
                      textprops={'fontsize': 10})

        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)

    @staticmethod
    def club_small_categories(data, threshold=PIE_CHART_OTHERS_THRESHOLD):
        """Clubs less than threshold percent categories into others
        Args:
            data(:obj): Dataframe of data and labels
            threshold(:int): Threshold of small categories to be clubbed
        Returns:
            Dataframe with less than threshold percent categories clubbed

        """
        if not threshold and threshold != 0:
            threshold = PIE_CHART_OTHERS_THRESHOLD
        total = data[0].sum()
        data["percent"] = round(data[0] / total * 100, utils.DECIMAL_PRECISION)
        row_data = dict()
        row_data[0] = data[data["percent"] < threshold][
            0].sum()
        row_data[1] = "Others"
        data = data[data["percent"] > threshold]
        if round(row_data[0], utils.DECIMAL_PRECISION) > 0:
            data = data.append(row_data, ignore_index=True)
        return data
